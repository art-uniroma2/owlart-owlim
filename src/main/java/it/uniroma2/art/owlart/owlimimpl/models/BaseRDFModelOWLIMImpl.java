package it.uniroma2.art.owlart.owlimimpl.models;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.sail.SailException;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.sesame2impl.models.BaseRDFModelSesame2Impl;

public class BaseRDFModelOWLIMImpl extends BaseRDFModelSesame2Impl {

	public BaseRDFModelOWLIMImpl(Repository repo, boolean rdfsReasoning, boolean directTypeReasoning)
			throws SailException, RepositoryException, ModelCreationException {
		super(repo, rdfsReasoning, directTypeReasoning);
	}

}
