/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (OWLIM Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API (OWLIM Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.owlimimpl.factory;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.LinkedDataResolver;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.owlimimpl.models.BaseRDFModelOWLIMImpl;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMInMemoryModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMNonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.OWLModelSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.RDFModelSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.RDFSModelSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.SKOSModelSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.SKOSXLModelSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.TripleQueryModelHTTPConnectionSesame2Impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailException;
import org.openrdf.sail.config.SailConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ontotext.trree.owlim_ext.config.OWLIMSailConfig;
import com.ontotext.trree.owlim_ext.config.OWLIMSailFactory;
import com.ontotext.trree.owlim_ext.config.OWLIMSailSchema;

/**
 * This class implements {@link ModelFactory}<br/>
 * 
 * A fast and easy way to load an RDF/RDFS/OWL Model by using this OWLIM implementation is to wrap it through
 * the OWL API convenience class {@link OWLArtModelFactory}, which handles standard configuration of models of
 * the RDF family, like loading proper vocabularies, setting baseuri/defnamespace of the loaded model etc... <br/>
 * This is the code to do that:
 * 
 * <pre>
 * ModelFactory fact = OWLArtModelFactory.createModelFactory(&lt;an instance of this class&gt;);
 * </pre>
 * 
 * @author Manuel Fiorelli <fiorelli@info.uniroma2.it>
 * 
 */
public class ARTModelFactoryOWLIMImpl implements ModelFactory<OWLIMModelConfiguration> {

	private static final Logger logger = LoggerFactory.getLogger(ARTModelFactoryOWLIMImpl.class);

	private List<Class<? extends OWLIMModelConfiguration>> supportedConfigurationClasses;

	private boolean populatingW3CVocabularies;

	public ARTModelFactoryOWLIMImpl() {
		supportedConfigurationClasses = new ArrayList<Class<? extends OWLIMModelConfiguration>>();
		supportedConfigurationClasses.add(OWLIMPersistentInMemoryModelConfiguration.class);
		supportedConfigurationClasses.add(OWLIMNonPersistentInMemoryModelConfiguration.class);

		populatingW3CVocabularies = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#createModelConfigurationObject(java.lang.Class)
	 */
	public <MCImpl extends OWLIMModelConfiguration> MCImpl createModelConfigurationObject(
			Class<MCImpl> mcclass) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {

		logger.debug("creating ModelConfigurationObject");

		if (supportedConfigurationClasses.contains(mcclass)) {

			try {
				logger.debug("requested model configuration class: " + mcclass);

				MCImpl mConf = (MCImpl) mcclass.newInstance();

				return mConf;
			} catch (InstantiationException e) {
				throw new UnloadableModelConfigurationException(mcclass);
			} catch (IllegalAccessException e) {
				throw new UnloadableModelConfigurationException(mcclass);
			}

		} else {
			throw new UnsupportedModelConfigurationException(this, mcclass);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#getModelConfigurations()
	 */
	public Collection<Class<? extends OWLIMModelConfiguration>> getModelConfigurations() {
		return supportedConfigurationClasses;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFBaseModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> BaseRDFModelOWLIMImpl loadRDFBaseModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {		
		HashMap<URI, String> params = new HashMap<URI, String>();
		
		// See: http://owlim.ontotext.com/display/OWLIMv53/OWLIM-Lite+Configuration
		
		if (baseuri != null) {
			params.put(OWLIMSailSchema.baseURL, baseuri);
		}

		if (persistenceDirectory != null) {
			params.put(OWLIMSailSchema.storagefolder, persistenceDirectory);
		}

		if (conf instanceof OWLIMInMemoryModelConfiguration) {
			params.put(OWLIMSailSchema.repository_type, "in-memory-repository");
			if (conf instanceof OWLIMPersistentInMemoryModelConfiguration) {
				params.put(OWLIMSailSchema.noPersist, "false");
			} else {
				params.put(OWLIMSailSchema.noPersist, "true");
			}
		} else {
			throw new ModelCreationException("unknown configuration type: " + conf.getClass());
		}
		
		OWLIMSailFactory factory = new OWLIMSailFactory();
		OWLIMSailConfig owlimSailConfig = (OWLIMSailConfig) factory.getConfig();
		owlimSailConfig.setConfigParams(params);
		
		try {
			Sail sail = factory.getSail(owlimSailConfig);
			SailRepository sailRepository = new SailRepository(sail);
			sailRepository.initialize();

			return new BaseRDFModelOWLIMImpl(sailRepository, true, populatingW3CVocabularies);
		} catch (SailConfigException e) {
			throw new ModelCreationException(e.getMessage());
		} catch (RepositoryException e) {
			throw new ModelCreationException(e.getMessage());
		} catch (SailException e) {
			throw new ModelCreationException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> RDFModel loadRDFModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {
		BaseRDFModelOWLIMImpl baseRep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);

		try {
			return new RDFModelSesame2Impl(baseRep);
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFSModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> RDFSModel loadRDFSModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {
		BaseRDFModelOWLIMImpl baseRep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);

		try {
			return new RDFSModelSesame2Impl(baseRep);
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadOWLModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> OWLModel loadOWLModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {
		BaseRDFModelOWLIMImpl baseRep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);

		try {
			return new OWLModelSesame2Impl(baseRep);
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSKOSModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> SKOSModel loadSKOSModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {
		BaseRDFModelOWLIMImpl baseRep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);

		try {
			return new SKOSModelSesame2Impl(baseRep);
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSKOSXLModel(java.lang.String, java.lang.String,
	 * it.uniroma2.art.owlart.models.conf.ModelConfiguration)
	 */
	public <MCImpl extends OWLIMModelConfiguration> SKOSXLModel loadSKOSXLModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException {
		BaseRDFModelOWLIMImpl baseRep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);

		try {
			return new SKOSXLModelSesame2Impl(baseRep);
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadTripleQueryHTTPConnection(java.lang.String)
	 */
	public TripleQueryModelHTTPConnection loadTripleQueryHTTPConnection(String endpointURL)
			throws ModelCreationException {
		return new TripleQueryModelHTTPConnectionSesame2Impl(endpointURL);
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadLinkedDataResolver()
	 */
	public LinkedDataResolver loadLinkedDataResolver() {
		throw new UnsupportedOperationException("To be implemented");
	}

	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.ModelFactory#createLightweightRDFModel()
	 */
	public BaseRDFTripleModel createLightweightRDFModel() {
		throw new UnsupportedOperationException("To be implemented");
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unirom	@Override
	public LinkedDataResolver loadLinkedDataResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BaseRDFTripleModel createLightweightRDFModel() {
		// TODO Auto-generated method stub
		return null;
	}
a2.art.owlart.models.ModelFactory#closeModel(it.uniroma2.art.owlart.models.BaseRDFTripleModel)
	 */
	public void closeModel(BaseRDFTripleModel rep) throws ModelUpdateException {
		rep.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#setPopulatingW3CVocabularies(boolean)
	 */
	public void setPopulatingW3CVocabularies(boolean pref) {
		populatingW3CVocabularies = pref;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#isPopulatingW3CVocabularies()
	 */
	public boolean isPopulatingW3CVocabularies() {
		return populatingW3CVocabularies;
	}

}
