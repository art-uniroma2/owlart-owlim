package it.uniroma2.art.owlart.owlimimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;


public class OWLIMPersistentInMemoryModelConfiguration extends OWLIMInMemoryModelConfiguration implements PersistenceModelConfiguration {

	public String getShortName() {
		return "in memory / persistent";
	}

	public boolean isPersistent() {
		return true;
	}

}
