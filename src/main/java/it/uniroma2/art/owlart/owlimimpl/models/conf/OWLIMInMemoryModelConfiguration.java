package it.uniroma2.art.owlart.owlimimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;


public abstract class OWLIMInMemoryModelConfiguration extends ModelConfigurationImpl implements OWLIMModelConfiguration {

	@ModelConfigurationParameter(description = "Ruleset to load; defaults to \"owl.host\"")
	public String ruleset = "owl-horst";

}
