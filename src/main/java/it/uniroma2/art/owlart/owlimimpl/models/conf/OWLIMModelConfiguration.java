package it.uniroma2.art.owlart.owlimimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfiguration;

public interface OWLIMModelConfiguration extends ModelConfiguration{
}
