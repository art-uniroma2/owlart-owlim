package it.uniroma2.art.owlart.owlimimpl.utilities.transform;

import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.util.ModelUtil;

import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.sesame2impl.model.ARTStatementSesame2Impl;
import it.uniroma2.art.owlart.utilites.transform.ModelComparator;

public class Sesame2ModelComparator implements ModelComparator {
	
	public boolean equals(RDFModel firstModel, RDFModel secondModel) throws Exception {
		ARTStatementIterator it = firstModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		Set<Statement> firstSet = new HashSet<Statement>();
		while (it.hasNext()) {
			firstSet.add(((ARTStatementSesame2Impl)it.next()).getSesameStatement());
		}
		it.close();
		
		it = secondModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		Set<Statement> secondSet = new HashSet<Statement>();
		while (it.hasNext()) {
			secondSet.add(((ARTStatementSesame2Impl)it.next()).getSesameStatement());
		}
		it.close();
		
		return ModelUtil.equals(firstSet, secondSet);

	}
}
