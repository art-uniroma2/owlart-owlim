package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.models.SPARQLConnectionTest;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import org.junit.BeforeClass;

public class SPARQLConnectionOWLIMImplTest extends SPARQLConnectionTest {

	@BeforeClass
	public static void initialize() {
		SPARQLConnectionTest.initializeModelFactory(new ARTModelFactoryOWLIMImpl());
	}
	
	
	
}
