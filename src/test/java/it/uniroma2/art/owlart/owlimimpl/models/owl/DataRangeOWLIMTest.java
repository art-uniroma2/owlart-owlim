package it.uniroma2.art.owlart.owlimimpl.models.owl;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.owl.DataRangeTest;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class DataRangeOWLIMTest extends DataRangeTest {

	@BeforeClass
	public static void loadRepository() throws Exception {
		DataRangeTest.initializeTest(new ARTModelFactoryOWLIMImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
	
	
	
	
}
