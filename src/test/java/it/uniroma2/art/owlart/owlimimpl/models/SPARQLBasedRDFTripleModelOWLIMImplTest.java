package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.models.SPARQLBasedRDFTripleModelImplTest;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import org.junit.BeforeClass;

public class SPARQLBasedRDFTripleModelOWLIMImplTest extends SPARQLBasedRDFTripleModelImplTest {

	@BeforeClass
	public static void initialize() {
		SPARQLBasedRDFTripleModelImplTest.initializeModelFactory(new ARTModelFactoryOWLIMImpl());		
	}
	
	
	
}
