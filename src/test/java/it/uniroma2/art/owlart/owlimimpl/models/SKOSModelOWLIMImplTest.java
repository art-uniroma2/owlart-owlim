package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.SKOSModelTest;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * basic OWLIM implementation for unit tests of OWL ART API
 * 
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 * 
 */
public class SKOSModelOWLIMImplTest extends SKOSModelTest {
	
	@BeforeClass
	public static void loadRepository() throws Exception {
		SKOSModelTest.initializeTest(new ARTModelFactoryOWLIMImpl(),false);
	}

	@AfterClass
	public static void classTearDown() {
		try {			
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}

	}

}
