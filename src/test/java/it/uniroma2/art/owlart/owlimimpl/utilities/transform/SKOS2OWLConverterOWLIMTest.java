package it.uniroma2.art.owlart.owlimimpl.utilities.transform;

import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMNonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.utilities.transform.Sesame2ModelComparator;
import it.uniroma2.art.owlart.utilites.transform.SKOS2OWLConverterTest;

import org.junit.Before;

public class SKOS2OWLConverterOWLIMTest extends SKOS2OWLConverterTest {
	@Before
	public void initializeTest() throws Exception {
		ARTModelFactoryOWLIMImpl factImpl = new ARTModelFactoryOWLIMImpl();
		
		OWLIMNonPersistentInMemoryModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(OWLIMNonPersistentInMemoryModelConfiguration.class);
		// modelConf has inference enabled by default
		
		OWLIMNonPersistentInMemoryModelConfiguration modelConf2 = factImpl
				.createModelConfigurationObject(OWLIMNonPersistentInMemoryModelConfiguration.class);
		modelConf2.ruleset = "empty";
		
		OWLIMNonPersistentInMemoryModelConfiguration modelConf3 = factImpl
				.createModelConfigurationObject(OWLIMNonPersistentInMemoryModelConfiguration.class);
		modelConf3.ruleset = "empty";

		
		initializeTest(new Sesame2ModelComparator(), factImpl, modelConf, modelConf2, modelConf3);
	}
}
