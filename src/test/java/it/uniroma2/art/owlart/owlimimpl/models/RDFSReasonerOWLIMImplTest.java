package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.RDFSReasonerTest;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMInMemoryModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMNonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.models.conf.OWLIMPersistentInMemoryModelConfiguration;

public class RDFSReasonerOWLIMImplTest extends RDFSReasonerTest<OWLIMModelConfiguration> {

	public ARTModelFactoryOWLIMImpl getModelFactory() {
		return new ARTModelFactoryOWLIMImpl();
	}

	public OWLIMModelConfiguration getNonReasoningModelConfiguration(
			ModelFactory<OWLIMModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		OWLIMInMemoryModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(OWLIMNonPersistentInMemoryModelConfiguration.class);
		modelConf.ruleset = "empty";
		return modelConf;
	}

	public OWLIMModelConfiguration getReasoningModelConfiguration(
			ModelFactory<OWLIMModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		// default configuration covers RDFS reasoning
		return factImpl.createModelConfigurationObject(OWLIMNonPersistentInMemoryModelConfiguration.class);
	}

}
