/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.EqualsOfRDFResourcesFactoryOnlyTest;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * OWLIM implementation of {@link EqualsOfRDFResourcesFactoryOnlyTest}.
 */
public class EqualsOfRDFResourcesFactoryOWLIMImplTest extends EqualsOfRDFResourcesFactoryOnlyTest {

	private static RDFModel model;

	@BeforeClass
	public static void setUp() throws ModelCreationException {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(new ARTModelFactoryOWLIMImpl());
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);

		setNodeFactory(model);
	}

	@AfterClass
	public static void classTearDown() {
		try {
			model.close();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			memStoreFile.delete();
			System.out.println("classTearDown: repository file deleted");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("classTearDown: failed to close the repository");
		}
	}
}
