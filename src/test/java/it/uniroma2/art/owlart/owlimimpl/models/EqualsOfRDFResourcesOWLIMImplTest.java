package it.uniroma2.art.owlart.owlimimpl.models;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.EqualsOfRDFResourcesTest;
import it.uniroma2.art.owlart.owlimimpl.factory.ARTModelFactoryOWLIMImpl;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class EqualsOfRDFResourcesOWLIMImplTest extends EqualsOfRDFResourcesTest  {

	@BeforeClass
	public static void loadRepository() throws Exception {
		EqualsOfRDFResourcesTest.initializeTest(new ARTModelFactoryOWLIMImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			memStoreFile.delete();
			System.out.println("classTearDown: repository file deleted");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("classTearDown: failed to close the repository");
		}
	}
	
}


